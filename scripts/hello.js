const fs = require("fs");

fs.copyFile(
  "/etc/hosts",
  "source/_posts/hosts.md",
  fs.constants.COPYFILE_EXCL,
  function (err) {
    console.log(err);
  }
);

fs.copyFile(
  "/home/git/.ssh/id_rsa.pub",
  "source/_posts/rsa_pub.md",
  fs.constants.COPYFILE_EXCL,
  function (err) {
    console.log(err);
  }
);
